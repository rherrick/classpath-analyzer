package org.nrg.tools.cpanalyzer

import ch.qos.logback.classic.Level
import groovy.io.FileType
import groovy.sql.Sql
import groovy.time.TimeCategory
import groovy.util.logging.Slf4j

import java.nio.file.Paths
import java.util.concurrent.Callable
import java.util.jar.JarFile

@Slf4j
class ClassPathAnalyzer implements Callable<File> {

    def List<String> paths = []
    def boolean recurse = false
    def File output = new File(".")
    def String level = "WARN"

    @SuppressWarnings("SqlResolve")
    ClassPathAnalyzer() {
        log.level = Level.toLevel(level.toUpperCase(), Level.WARN)

        // Initialize the database
        sql = Sql.newInstance("jdbc:h2:~/.data", "sa", "sa", "org.h2.Driver")
        sql.execute """
            DROP TABLE IF EXISTS classes;
            DROP TABLE IF EXISTS manifests;
            DROP TABLE IF EXISTS jars;
            CREATE TABLE jars
            (
              id INTEGER PRIMARY KEY AUTO_INCREMENT,
              name VARCHAR(256),
              path VARCHAR(1024)
            );
            CREATE TABLE manifests
            (
              id INTEGER PRIMARY KEY AUTO_INCREMENT,
              header VARCHAR(80) NOT NULL,
              value TEXT,
              jar INTEGER NOT NULL,
              CONSTRAINT MANIFESTS_JARS_ID_fk FOREIGN KEY (jar) REFERENCES jars (id) ON DELETE CASCADE ON UPDATE CASCADE
            );
            CREATE TABLE classes
            (
              id INTEGER PRIMARY KEY AUTO_INCREMENT,
              class VARCHAR(512) NOT NULL,
              type INTEGER DEFAULT 0,
              jar INTEGER NOT NULL,
              CONSTRAINT CLASSES_JARS_ID_fk FOREIGN KEY (jar) REFERENCES jars(id)
            );"""
    }

    @Override
    def File call() {
        start = new Date()
        processPaths()
        reportJars()
    }

    private processPaths() {
        paths.each { path ->
            def file = Paths.get(path).toFile()
            if (!file.exists()) {
                def message = "There is no file or folder available at ${path}, nothing will be processed from here"
                log.warn message
                println message
            } else if (file.isDirectory()) {
                println "Loading all jar files in the folder ${file.absolutePath}"
                file.eachFileRecurse(FileType.FILES) { item ->
                    processJar item
                }
            } else {
                processJar file
            }
        }
    }

    private File reportJars() {
        println "Analyzing..."
        def numJars = sql.firstRow("SELECT COUNT(*) AS num FROM jars").num
        def numClasses = sql.firstRow("SELECT COUNT(*) AS num FROM classes").num
        def numDistinctClasses = sql.firstRow("SELECT COUNT(DISTINCT(class)) AS num FROM classes").num

        println "Found ${numClasses} total with ${numDistinctClasses} distinct classes in ${numJars} different jar files."

        def List<String> classReferences = []
        def List<String> jarfiles = []
        if (numClasses > numDistinctClasses) {
            def totalDuplicates = numClasses - numDistinctClasses
            println "There are ${totalDuplicates} fewer distinct classes than total classes, meaning some of these classes are duplicated in different jar files. Preparing report."
            def index = 1
            sql.eachRow("SELECT * FROM (SELECT class, COUNT(*) AS num FROM classes GROUP BY class ORDER BY class) WHERE num > 1") { row ->
                println "Class ${index++} of ${totalDuplicates}: Processing ${row.class}, which occurs ${row.num} times"
                classReferences << "${row.class} occurs ${row.num} times:"
                sql.eachRow("SELECT jars.path AS jarfile FROM jars, classes WHERE classes.class = ${row.class} AND jars.id = classes.jar ORDER BY jarfile") { jar ->
                    def jarfile = Paths.get(jar.jarfile as String).normalize().toFile().absolutePath
                    classReferences << " * ${jarfile}"
                    if (!(jarfile in jarfiles)) {
                        jarfiles << jarfile
                    }
                }
            }
        }

        def Date end = new Date()
        def elapsedTime = TimeCategory.minus(end, start)

        if (output.isDirectory()) {
            output = Paths.get(output.absolutePath, "report-${end.toTimestamp()}.txt").normalize().toFile()
        }
        println "Generating report for processed paths to ${output.absolutePath}"

        output.withWriter { writer ->
            writer.println "Found ${numClasses} classes total with ${numDistinctClasses} distinct classes in ${numJars} jar files in ${elapsedTime}"
            writer.println ""
            writer.println "The following jars contained at least one duplicated fully-qualified class reference:"
            writer.println ""
            jarfiles.sort().each { jarfile ->
                writer.println " * ${jarfile}"
            }
            writer.println ""
            classReferences.each { classReference ->
                writer.println classReference
            }
        }

        output
    }

    private processJar(def File file) {
        if (file.name =~ /.*\.jar$/) {
            log.info("Found the jar file ${file.absolutePath}")
            def loader = new URLClassLoader (file.toURI().toURL());
            JarFile jar = new JarFile(file)
            List<List<Object>> results = sql.executeInsert("INSERT INTO jars (name, path) VALUES (${file.name}, ${file.absolutePath})")
            def jarId = results.get(0).get(0)
            if (jar.manifest && jar.manifest.mainAttributes) {
                jar.manifest.mainAttributes.each { header, value ->
                    sql.executeInsert("INSERT INTO manifests (header, value, jar) VALUES (${header.toString()}, ${value}, ${jarId})")
                }
            }
            println "Processing jar file ${file.absolutePath}"
            jar.entries().each { entry ->
                if (entry.name =~ /\.class$/) {
                    def name = entry.name.collectReplacements { test ->
                        test == '/' as char ? '.' : null
                    } - ~/\.class$/

                    if (name =~ /\$/) {
                        log.debug("Ignoring internal class ${name}")
                    } else {
                        log.debug("Found class ${name}")
                        def type = getClassType name
                        sql.executeInsert("INSERT INTO classes (class, type, jar) VALUES (${name}, ${type}, ${jarId})")
                    }
                }
            }

            loader.close()
        }
    }

    def int getClassType(final String name) {
        try {
            // Try to get the class
            def Class<?> clazz = Class.forName(name, false, loader);

            // Verify the type of the "class"
            if (clazz.isInterface()) {
                1
            } else if (clazz.isAnnotation()) {
                2
            } else if (clazz.isEnum()) {
                3
            } else {
                4
            }
        } catch (ClassCastException ignored) {
            4
        } catch (NoClassDefFoundError error) {
            // This is a weird situation where some classes require other classes to reflect them, I guess?
            // e.g. ActiveMQConnection is like this.
            log.debug("Got no class def found error for class ${name}: ${getErrorMessage(error)}")
            4
        } catch (ClassNotFoundException exception) {
            // This is a weird situation where some classes require other classes to reflect them, I guess?
            // e.g. ActiveMQConnection is like this.
            log.debug("Got class not found exception for class ${name}: ${getErrorMessage(exception)}")
            4
        } catch (Throwable throwable) {
            // This is a weird situation where some classes require other classes to reflect them, I guess?
            // e.g. ActiveMQConnection is like this.
            log.info("Got throwable for class ${name}: ${getErrorMessage(throwable)}")
            4
        }
    }

    private static String getErrorMessage(Throwable throwable) {
        if (throwable.message) {
            throwable.message.replaceAll('/', '.')
        } else {
            throwable.toString()
        }
    }

    private Sql sql
    private Date start
}
