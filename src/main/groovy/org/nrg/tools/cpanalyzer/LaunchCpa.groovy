package org.nrg.tools.cpanalyzer

import groovyjarjarcommonscli.Option

import java.nio.file.Paths

def CliBuilder builder = new CliBuilder()
builder.s longOpt: 'source', required: true, args: Option.UNLIMITED_VALUES, valueSeparator: ',' as char, argName: 'location(s)', 'Parses jar files in indicated location', type: String
// builder.r longOpt: 'recurse', 'If specified, indicates CPA should recurse into any subfolders of the source folder (default is false)', type: boolean
builder.o longOpt: 'output', args: 1, argName: 'location', 'Generate report at specified location (will overwrite existing any files)', type: String
builder.l longOpt: 'logging', args: 1, argName: 'level', 'Specify the logging level. Can be one of TRACE, DEBUG, WARN, ERROR, or OFF.', type: String

def options = builder.parse args

def ClassPathAnalyzer cpa = new ClassPathAnalyzer()
cpa.paths.addAll options.ss
//if (options.r) {
//    cpa.recurse = options.r
//}
if (options.o) {
    cpa.output = Paths.get(options.o).toFile()
}
if (options.l) {
    cpa.level = options.l
}

def File report = cpa.call()
println "ClassPathAnalyzer execution completed. You can find the generated report at ${report.absolutePath}."
