package org.nrg.tools.cpanalyzer

import org.junit.Test

import java.nio.file.Paths

import static org.junit.Assert.assertTrue

class ClassPathAnalyzerTest {
    @Test
    def void testSimpleProcessing() {
        def path = Paths.get("src/test/resources/lib")
        assertTrue path.toFile().exists()
        assertTrue path.toFile().isDirectory()
        assertTrue path.toFile().list().length > 1
        def analyzer = new ClassPathAnalyzer()
        analyzer.paths << path.toString()
        def File report = analyzer.call()
        assertTrue report.exists()
    }
}
