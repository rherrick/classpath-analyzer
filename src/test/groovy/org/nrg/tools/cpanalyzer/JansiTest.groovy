package org.nrg.tools.cpanalyzer

import org.fusesource.jansi.AnsiConsole
import org.junit.After
import org.junit.Before
import org.junit.Test

import static org.fusesource.jansi.Ansi.*
import static org.fusesource.jansi.Ansi.Color.*

// This isn't really a unit test, but just a place for me to keep notes on Jansi, which I plan to integrate into the
// ClassPathAnalyzer code. Documentation and source available at https://github.com/fusesource/jansi.
public class JansiTest {
    @Before
    void setup() {
        AnsiConsole.systemInstall()
    }

    @After
    void teardown() {
        AnsiConsole.systemUninstall()
    }

    @Test
    void testPrintln() {
        // These two lines are functionally equivalent.
        println ansi().eraseScreen().fg(RED).a("Hello").fg(GREEN).a(" World").reset()
        println ansi().eraseScreen().render("@|red Hello|@ @|green World|@")
    }
}
